# Práctica de Modelos de Datos

## Enunciado

Una empresa multinacional nos ha solicitado una aplicación en Django para incluirla en su software de gestión, la cual debe realizar lo siguiente:

* Realizar control de los datos del empleado (nombre, DNI, dirección, email, puesto de trabajo, fábrica en la que trabaja).
* Control de los puestos de trabajo (cargo y descripción del cargo) y salarios asociados (bruto anual a cobrar, gratificación Junio, gratificación Diciembre).
* Control de las localizaciones de cada fábrica (nombre, dirección, código postal y ciudad).

## Objetivo de la práctica

Se debe definir un modelo entidad-relación del problema descrito en el enunciado y posteriormente definir los modelos con sus respectivas relaciones en un proyecto de Django. El motor de bases de datos a utilizar será PostgreSQL.

## Diagrama Entidad-Relación

```mermaid
erDiagram
	SALARY ||--|{ JOB: ""
	JOB ||--|{ EMPLOYEE: ""
	COUNTRY ||--|{ CITY: ""
	CITY ||--|{ FACTORY: ""
	FACTORY ||--|{ EMPLOYEE: ""

	EMPLOYEE {
		int id PK
		string id_number
		string first_name
		string last_name
		string email
		string address
	}
	JOB {
		int id PK
		string title
		string description
	}
	SALARY {
		int id PK
		int amount
		boolean extra_december
		boolean extra_june
	}
	FACTORY {
		int id PK
		string name
		string address
		string zip_code
	}
	CITY {
		int id PK
		string name
	}
	COUNTRY {
		int id PK
		string name
		int country_code
	}
```