# Curso 4: Django

## Contenidos del curso

* Tema 1: Introducción a Django
* Tema 2: Rutas y Plantillas
* Tema 3: Vistas y Modelos
* Tema 4: Formularios
* Tema 5: Panel de Administración
* Tema 6: Proyecto CRUD
* Tema 7: Utilidades
