from django.contrib import admin
from django.urls import path
from . import views
urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.about_me, name='about_me'),
    path('portfolio/', views.portfolio, name='portfolio')
]
