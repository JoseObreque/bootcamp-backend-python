from django.shortcuts import render

def about_me(request):
    return render(request, 'about_me.html', {})

def portfolio(request):
    context = {
        'num_examples': range(1, 10),
        'example_img_url': 'https://files.realpython.com/media/Get-Started-With-Django_Watermarked.15a1e05597bc.jpg'
    }
    return render(request, 'portfolio.html', context)