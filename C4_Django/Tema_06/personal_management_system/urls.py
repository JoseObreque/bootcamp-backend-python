from django.contrib import admin
from django.urls import path, include
from . import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('contact/', include('contact_list.urls')),
    path('todo/', include('todo_list.urls')),
    path('', views.index, name="index")
]
