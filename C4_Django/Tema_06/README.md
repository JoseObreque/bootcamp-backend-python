# Proyecto Final: Gestor Personal

## Enunciado
Crear una aplicación web que permita realizar la gestión
personal en dos apartados principales:

* Agenda de Contactos
* Lista de Tareas Pendientes

## Requisitos Generales
Para el desarrollo de esta actividad, se deben tener en cuenta los
siguientes requerimientos generales:

* Utilizar lenguaje Python 3+
* Utilizar el framework Django
* Para la persistencia de datos, se debe utilizar PostgreSQL
* El front-end debe trabajarse utilizando el motor de plantillas de
Django: Django-Templates

## Requisitos Específicos

### Agenda de Contactos

Para el desarrollo de la funcionalidad de la agenda de contactos, se debe
tener en cuenta lo siguiente:

* Se deben poder realizar acciones CRUD sobre los contactos.
* Se debe disponer de un campo de búsqueda por nombre y/o apellido de un contacto.
* Se debe disponer de un índice alfabético para filtrar por la 
inicial del nombre del contacto.
* Debe ser posible ver un listado completo de los contactos, así como
también disponer de una vista detallada de cada uno de ellos.

### Lista de Tareas pendientes

Para el desarrollo de la funcionalidad de la lista de tareas pendientes, se
debe tener en cuenta lo siguiente:

* Se deben poder realizar acciones CRUD sobre las tareas.
* Se debe disponer de un campo de búsqueda por título de la tarea.
* Debe ser posible ver un listado completo de las tareas, así como
también disponer de una vista detallada de cada una de ellas.