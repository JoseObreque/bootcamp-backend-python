from django import forms
from django.forms import ModelForm
from .models import ToDo


class ToDoForm(ModelForm):
    class Meta:
        model = ToDo
        fields = '__all__'
        widgets = {
            'title': forms.TextInput(attrs={
                'class': 'form-control'
            }),
            'description': forms.Textarea(attrs={
                'class': 'form-control',
                'rows': 6
            }),
            'start_date': forms.DateInput(attrs={
                'class': 'form-control'
            }),
            'due_date': forms.DateInput(attrs={
                'class': 'form-control'
            }),
            'priority': forms.Select(attrs={
                'class': 'form-select'
            })
        }
        labels = {
            'title': 'Título',
            'description': 'Descripción',
            'start_date': 'Fecha de Inicio',
            'due_date': 'Plazo Límite',
            'priority': 'Prioridad'
        }
