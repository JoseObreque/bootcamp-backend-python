from django.db import models
from datetime import date


class Priorities(models.IntegerChoices):
    MUY_BAJA = 1
    BAJA = 2
    MEDIA = 3
    ALTA = 4
    MUY_ALTA = 5


class ToDo(models.Model):
    title = models.CharField(
        max_length=100,
        blank=False,
        null=False
    )
    description = models.TextField(
        blank=True,
        null=False,
        default=''
    )
    start_date = models.DateField(
        default=date.today(),
        blank=False,
        null=False
    )
    due_date = models.DateField(
        blank=True,
        null=True
    )
    priority = models.IntegerField(
        choices=Priorities.choices,
        default=3,
        blank=False,
        null=False
    )

    def __str__(self):
        return self.title
