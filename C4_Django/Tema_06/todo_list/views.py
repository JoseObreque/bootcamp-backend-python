from django.shortcuts import render, redirect
from .models import ToDo
from .forms import ToDoForm


def index(request):
    todos = ToDo.objects.filter(
        title__icontains=request.GET.get('search', '')
    ).order_by('id')
    context = {
        'todos': todos
    }
    return render(request, 'todo_list/index.html', context)


def details(request, id):
    todo = ToDo.objects.get(id=id)
    context = {
        'todo': todo
    }
    return render(request, 'todo_list/detail.html', context)


def edit(request, id):
    todo = ToDo.objects.get(id=id)
    context = {
        'id': id
    }
    if request.method == 'GET':
        form = ToDoForm(instance=todo)
        context['form'] = form
        return render(request, 'todo_list/edit.html', context)

    if request.method == 'POST':
        form = ToDoForm(request.POST, instance=todo)
        if form.is_valid():
            form.save()
            return redirect('todo')
        else:
            context['form'] = form
            return render(request, 'todo_list/edit.html', context)


def create(request):
    if request.method == 'GET':
        form = ToDoForm()
        context = {
            'form': form
        }
        return render(request, 'todo_list/create.html', context)

    if request.method == 'POST':
        form = ToDoForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('todo')
        else:
            context = {
                'form': form
            }
            return render(request, 'todo_list/create.html', context)


def delete(request, id):
    todo = ToDo.objects.get(id=id)
    todo.delete()
    return redirect('todo')
