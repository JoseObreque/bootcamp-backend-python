from django.db import models
from datetime import date


class Contact(models.Model):
    name = models.CharField(
        max_length=30,
        blank=False,
        null=False
    )
    last_name = models.CharField(
        max_length=50,
        blank=True,
        null=False,
        default=''
    )
    phone_number = models.CharField(
        max_length=12,
        blank=True,
        null=False,
        default=''
    )
    cell_phone_number = models.CharField(
        max_length=12,
        blank=False,
        null=False
    )
    email = models.EmailField()
    company = models.CharField(
        max_length=20,
        blank=True,
        null=False,
        default=''
    )
    registration_date = models.DateField(
        default=date.today()
    )
    notes = models.TextField(
        blank=True,
        null=False,
        default=''
    )

    def __str__(self):
        return self.name
