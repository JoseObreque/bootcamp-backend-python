# Generated by Django 4.1.3 on 2022-11-26 17:49

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Contact',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=30)),
                ('last_name', models.CharField(blank=True, max_length=50)),
                ('phone_number', models.CharField(blank=True, max_length=12)),
                ('cell_phone_number', models.CharField(max_length=12)),
                ('email', models.EmailField(max_length=254)),
                ('company', models.CharField(blank=True, max_length=20)),
                ('registration_date', models.DateField(default=datetime.date(2022, 11, 26))),
                ('notes', models.TextField(blank=True)),
            ],
        ),
    ]
