from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name="contact"),
    path('<search_letter>', views.index, name="contact"),
    path('details/<int:id>', views.details, name="contact_details"),
    path('edit/<int:id>', views.edit, name="contact_edit"),
    path('create/', views.create, name="contact_create"),
    path('delete/<int:id>', views.delete, name="contact_delete")
]
