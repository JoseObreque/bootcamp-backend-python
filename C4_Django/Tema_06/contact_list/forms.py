from django import forms
from django.forms import ModelForm
from .models import Contact


class ContactForm(ModelForm):
    class Meta:
        model = Contact
        exclude = ('registration_date',)
        widgets = {
            'name': forms.TextInput(attrs={
                'class': 'form-control'
            }),
            'last_name': forms.TextInput(attrs={
                'class': 'form-control'
            }),
            'phone_number': forms.TextInput(attrs={
                'class': 'form-control'
            }),
            'cell_phone_number': forms.TextInput(attrs={
                'class': 'form-control'
            }),
            'email': forms.EmailInput(attrs={
                'class': 'form-control'
            }),
            'company': forms.TextInput(attrs={
                'class': 'form-control'
            }),
            'notes': forms.Textarea(attrs={
                'class': 'form-control',
                'rows': 6
            })
        }
        labels = {
            'name': 'Nombre',
            'last_name': 'Apellido',
            'phone_number': 'Teléfono Fijo',
            'cell_phone_number': 'Celular',
            'company': 'Compañía',
            'notes': 'Notas'
        }
