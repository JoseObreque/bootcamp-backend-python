import string
from django.db.models import Q
from django.shortcuts import render, redirect
from .models import Contact
from .forms import ContactForm


def index(request, search_letter=''):
    letters = list(string.ascii_uppercase)

    if search_letter:
        contacts = Contact.objects.filter(
            name__istartswith=search_letter
        )
    else:
        contacts = Contact.objects.filter(
            Q(name__icontains=request.GET.get('search', '')) |
            Q(last_name__icontains=request.GET.get('search', ''))
        ).order_by('id')

    context = {
        'contacts': contacts,
        'letters': letters
    }

    return render(request, 'contact_list/index.html', context)


def details(request, id):
    contact = Contact.objects.get(id=id)
    context = {
        'contact': contact
    }
    return render(request, 'contact_list/detail.html', context)


def edit(request, id):
    contact = Contact.objects.get(id=id)
    context = {
        'id': id
    }

    if request.method == 'GET':
        form = ContactForm(instance=contact)
        context['form'] = form
        return render(request, 'contact_list/edit.html', context)

    if request.method == 'POST':
        form = ContactForm(request.POST, instance=contact)
        context['form'] = form
        if form.is_valid():
            form.save()
            return redirect('contact')
        else:
            return render(request, 'contact_list/edit.html', context)


def create(request):
    if request.method == 'GET':
        form = ContactForm()
        context = {
            'form': form
        }
        return render(request, 'contact_list/create.html', context)

    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('contact')
        else:
            context = {
                'form': form
            }
            return render(request, 'contact_list/create.html', context)


def delete(request, id):
    contact = Contact.objects.get(id=id)
    contact.delete()
    return redirect('contact')
