# Práctica de Formularios

## Enunciado

Se requiere construir un formulario para ingreso de nuevos productos
al inventario de una tienda/comercio. Cada producto debe incluir los siguientes
campos:

* Nombre del producto
* Descripción corta
* Descripción detallada
* Stock
